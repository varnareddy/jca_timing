
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Properties;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class JCATime {
	Constants constant = new Constants();

	public static void main(String[] args) throws Exception {
		
		// System Properties
		Properties properties = System.getProperties();
	        System.out.println("OS and Version : "+properties.get("os.name")+properties.get("os.version"));
	        System.out.println("Java Version : "+properties.get("java.version"));
	        System.out.println("Data Model : "+properties.get("sun.arch.data.model"));
	        System.out.println("OS Architecture : "+properties.get("os.arch"));
	        System.out.println("Java Version : "+properties.get("java.specification.version"));
	     
	    //Object creations
		JCATime jct = new JCATime(); // object for calling encryption methods written
		Constants constantmain = new Constants();
		
		//Variable Declarations
		byte[] originaltext = new byte[constantmain.InputMax];
		
		// file data transfer to input byte array -Start
		
		Path inputfileloc = Paths.get(constantmain.inputFile);//for testing any other file change the inputFile string in  constant file 
		originaltext = Files.readAllBytes(inputfileloc);
		
		// file data transfer to input byte array - End

		// Encryption Throughput of Algorithm1 &2
		 jct.singleKeyEncryptionThroughput(constantmain.Algorithm1, originaltext);
		 jct.singleKeyEncryptionThroughput(constantmain.Algorithm2, originaltext);

		// Time to key Testing rate for Algorithm 1&2
		 jct.KeyTestingRate(constantmain.Algorithm1,constantmain.inputFile);
		 jct.KeyTestingRate(constantmain.Algorithm2,constantmain.inputFile);
		 
	}

	public void singleKeyEncryptionThroughput(String Algorithm, byte[] inputtext) {
		long startTimeAlgo = System.nanoTime(); // Algorithm --> Encryption Process Start here

			Cipher Algocipher = null;
			try {
				Algocipher = Cipher.getInstance((Algorithm == constant.Algorithm1)?constant.Algo1instance:constant.Algo2instance);
			} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {				
				e.printStackTrace();
			}

			KeyGenerator AlgoKeyGenerator = null;
			try {
				AlgoKeyGenerator = KeyGenerator.getInstance((Algorithm == constant.Algorithm1)?constant.Algorithm1:constant.Algorithm2);
			} catch (NoSuchAlgorithmException e) {
				
				System.out.println("No Such ALgorithm Exist");
				
			}
			SecureRandom AlgoSecureRandom = new SecureRandom();
			int AlgoKeySize = (Algorithm == constant.Algorithm1)?constant.Algo1Key:constant.Algo2Key;
			AlgoKeyGenerator.init(AlgoKeySize, AlgoSecureRandom);

			SecretKey AlgoSecretKey = AlgoKeyGenerator.generateKey();
			byte[] Algo1iv = (Algorithm == constant.Algorithm1)?constant.Algo1IntVector:constant.Algo2IntVector;
			IvParameterSpec Algo1ivspec = new IvParameterSpec(Algo1iv);
			try {
				Algocipher.init(Cipher.ENCRYPT_MODE, AlgoSecretKey, Algo1ivspec);
			} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {

				e.printStackTrace();
			}

			byte[] plainText = inputtext;
			try {
				// repeating the same input to encrypt ---to make large size input file
				for (int i = 0; i < constant.inputloopIterator; i++) {
					byte[] cipherText = Algocipher.doFinal(plainText);
				}
			} catch (IllegalBlockSizeException | BadPaddingException e) {

				e.printStackTrace();
			}
			long endTimeAlgo = System.nanoTime();// Algorithm --> Encryption Process Ends Here

			// Calculating total execution time for ALgorithm1 Encryption process
			int TotalTimeforAlgo = (int) ((endTimeAlgo - startTimeAlgo) / (Math.pow(10, 9)));
			// Calculating input text multiplied by number of repeated encryptions and converting to MBs...
			double inputtextInMB = ((inputtext.length) * constant.inputloopIterator) / (Math.pow(10, 6));

			System.out.println(" -------------" + Algorithm + " Characteristics" + "-------------");
			System.out.println();
			System.out.println("Total size of input text :" + inputtextInMB + " MB");
			System.out.println("Total time taken to Encrypt the file : " + TotalTimeforAlgo + " Seconds");
			System.out.println("Encryption Throughput of Algorithm "+Algorithm+": " +(inputtextInMB / TotalTimeforAlgo) + " MB/Sec");
			System.out.println();
		
	}
		public void KeyTestingRate ( String AlgorithmKeyTesting, String originaltext) throws IllegalBlockSizeException, BadPaddingException {
			
			long KeystartTime = System.nanoTime(); // Algorithm --> Encryption Process Start here
			int blocksize = (AlgorithmKeyTesting == constant.Algorithm1)?constant.Algo1blocksize:constant.Algo2blocksize;
			int TotalTimeKeyforAlgo;
			byte[] AlgoBlock = new byte[blocksize];
			int length,counter =0,inputsize=0;
		
			FileInputStream in = null;
			try {
				in = new FileInputStream(originaltext);
			} catch (FileNotFoundException e3) {
				System.out.println("File not Found");
			}
			byte[] ciphertext;
			
			Cipher EncryptCipher = null;
			try {
				EncryptCipher = (AlgorithmKeyTesting == constant.Algorithm1)?Cipher.getInstance(constant.Algo1instance):Cipher.getInstance(constant.Algo2instance);
			} catch (NoSuchAlgorithmException | NoSuchPaddingException e2) {
				e2.printStackTrace();
			}
			try {
				in.read(AlgoBlock);
			} catch (IOException e2) {
				e2.printStackTrace();
			}
				
				for(int i=0;i<constant.keyloopiterator;i++)
				{
					// Symmetric Key genearation
					KeyGenerator Algokeygen = null;
					try {
						Algokeygen = (AlgorithmKeyTesting == constant.Algorithm1)?KeyGenerator.getInstance(constant.Algorithm1):KeyGenerator.getInstance(constant.Algorithm2);
					} catch (NoSuchAlgorithmException e1) {
						System.out.println("No Such ALgorithm Exist");
					}
					
					// Random key values generation
					SecureRandom securerand = new SecureRandom();
					
					// intializing key size and also key generator
					int Algokeysize = (AlgorithmKeyTesting == constant.Algorithm1)?constant.Algo1Key:constant.Algo2Key;
					Algokeygen.init(Algokeysize, securerand);
					SecretKey Algosymmetrickey = Algokeygen.generateKey();
					byte[] Algoiv = (AlgorithmKeyTesting == constant.Algorithm1)?constant.Algo1IntVector:constant.Algo2IntVector;
					IvParameterSpec Algoivspec = new IvParameterSpec(Algoiv);
					
					// initialize the cipher
					try {
						EncryptCipher.init(Cipher.ENCRYPT_MODE, Algosymmetrickey, Algoivspec);
					} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
						e.printStackTrace();
					}
					ciphertext = EncryptCipher.update(AlgoBlock, 0, AlgoBlock.length);
					
				}
			
			try {
				ciphertext = EncryptCipher.doFinal();
			} catch (IllegalBlockSizeException | BadPaddingException e) {
				e.printStackTrace();
			}
			long KeyendTime = System.nanoTime(); // Algorithm --> Encryption Process Ends Here
			TotalTimeKeyforAlgo = (int) ((KeyendTime - KeystartTime)/(Math.pow(10, 9))) ; 
			
			System.out.println(" -------------" + AlgorithmKeyTesting + " Characteristics" + "-------------");
			System.out.println();
			System.out.println("Total time taken for "+AlgorithmKeyTesting+": "+TotalTimeKeyforAlgo+" Sec");
			System.out.println("Number of keys used : "+constant.keyloopiterator/TotalTimeKeyforAlgo+" Keys/Sec");
			
			

	}

	}

